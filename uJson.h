#ifndef JSON_H
#define JSON_H

#include "JsonRoot.h"
#include "JsonArray.h"
#include "JsonObject.h"
#include "JsonString.h"
#include "JsonNumber.h"
#include "JsonBool.h"
#include "JsonNull.h"
#include "JsonLoader.h"
#include "JsonException.h"

#define CHECK_ASSIGN_TYPE(dst,src,type)    (((((dst)=((src)->getType()==type)?dynamic_cast<__typeof__(dst)>(src):NULL))==NULL)?-1:0)
#define CHECK_ASSIGN_SRCNULL(dst, src,type) ((src==NULL)?-1:CHECK_ASSIGN_TYPE(dst,src,type))
#define CHECK_ASSIGN_OBJECT(dst, src)       CHECK_ASSIGN_SRCNULL(dst,src, JSON_OBJECT)
#define CHECK_ASSIGN_ARRAY(dst, src)        CHECK_ASSIGN_SRCNULL(dst,src, JSON_ARRAY)                    
#define CHECK_ASSIGN_NUMBER(dst, src)       CHECK_ASSIGN_SRCNULL(dst,src, JSON_NUMBER)
#define CHECK_ASSIGN_STRING(dst, src)       CHECK_ASSIGN_SRCNULL(dst,src, JSON_STRING)
#define CHECK_ASSIGN_NULL(dst, src)        CHECK_ASSIGN_SRCNULL(dst,src, JSON_NULL)
#define CHECK_ASSIGN_BOOL(dst, src)        CHECK_ASSIGN_SRCNULL(dst,src, JSON_BOOL)


class uJson{
 public:
  explicit uJson(JsonRoot *pRoot);
  uJson();
  void cleanup();
  int count();
  JsonNodeType getType();
  uJson operator[] (int pIndex);
  uJson operator[] (std::string pName);
  uJson operator[] (const char *pName);
  operator int() const;
  operator double() const;
  operator std::string() const;
  operator bool() const;
  int operator=(int pValue);
  double operator=(double pValue);
  std::string & operator=(std::string pValue);
  std::string & operator=(const char *pValue);
  bool operator=(bool pValue);
  BaseJsonNode *getNode();
  std::string serialize();
  bool contains(std::string pKey);
 private:
  BaseJsonNode *mNode;
};

  
#endif


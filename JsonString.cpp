#include "JsonString.h"

JsonString::JsonString(std::string &pValue){
  unsigned int c;
  if(pValue[0] == '\"'){
    mValue = "";
    for(c = 1; c < pValue.size()-1; c++){
      mValue += pValue[c];
    }
  }else{
    mValue = pValue;
  }
}

JsonString::JsonString(const char *pValue){
  unsigned int c;
  std::string value;
  value = std::string(pValue);
  if(value[0] == '\"'){
    mValue = "";
    for(c = 1; c < value.size()-1; c++){
      mValue += value[c];
    }
  }else{
    mValue = value;
  }  
}

JsonNodeType JsonString::getType() const{
  return JSON_STRING;
}

std::string JsonString::serialize(){
  return "\"" + mValue + "\"";  
}

std::string JsonString::value() const{
  return mValue;
}


std::string & JsonString::setValue(std::string pValue){
  mValue = pValue;
  return mValue;
}

std::string & JsonString::setValue(const char *pValue){
  mValue = std::string(pValue);
  return mValue;
}

bool JsonString::nodeEqual(BaseJsonNode *pNode){
  JsonString *tmp;
  tmp = (JsonString*)pNode;
  return mValue == tmp->mValue;
}

BaseJsonNode *JsonString::copyInternal() const{
  JsonString *js;
  js = new JsonString("");
  js->mValue = mValue;
  return js;
}

std::string JsonString::valueEscaped() const{
  std::string ret;
  unsigned int c;
  char nx; 
  ret = "";
  for(c = 0; c < mValue.length(); c++){
    if(mValue[c] == '\\'){
      if(c == mValue.length() - 1){
	return ret;
      }
      if(mValue[c+1] == 'n'){
	nx = '\n';
      }else if(mValue[c+1] == '\"'){
	nx = '\"';
      }else if(mValue[c+1] == '\r'){
	nx = '\r';	
      }else if(mValue[c+1] == '\t'){
	nx = '\t';
      }else if(mValue[c+1] == '\b'){
	nx = '\b';
      }else if(mValue[c+1] == '\\'){
	nx = '\\';	
      }else if(mValue[c+1] == '\''){
	nx = '\'';
      }else if(mValue[c+1] == '\b'){
	nx = '\b';
      }else{
	nx = ' ';
      }
      c++;
    }else{
      nx = mValue[c];      
    }
    ret += nx;
  }
  return ret;    
}

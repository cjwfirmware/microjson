SRCS= JsonDriver.cpp JsonString.cpp JsonNumber.cpp JsonBool.cpp JsonNull.cpp JsonArray.cpp JsonObject.cpp BaseJsonList.cpp JsonObjEntry.cpp JsonRoot.cpp BaseJsonNode.cpp uJson.cpp JsonException.cpp JsonLoader.cpp
INSTALL_HEADERS= BaseJsonList.h BaseJsonNode.h JsonArray.h JsonBool.h JsonDriver.h uJson.h JsonNull.h JsonNumber.h JsonObject.h JsonObjEntry.h JsonRoot.h JsonScanner.h JsonString.h JsonException.h JsonParser.hpp location.hh stack.hh position.hh JsonLoader.h

TEST_SRCS= ThreadParseTest.cpp threadhammer.cpp
OBJS=$(patsubst %.cpp,%.o,$(SRCS))
TEST_OBJS=$(patsubst %.cpp,%.o,$(TEST_SRCS))
LYOBJS=JsonParser.o JsonScanner.o


CFLAGS= -g -O3 -Wall -fPIC
PREFIX ?= .
CXX ?= g++
AR ?= ar

TARGET_LIB=$(shell ls libmicrojson.*)

all: libmicrojson.so

static: libmicrojson.a

install: $(TARGET_LIB)
	mkdir -p $(PREFIX)/include
	mkdir -p $(PREFIX)/lib
	install -m 644 $(INSTALL_HEADERS) $(PREFIX)/include
	install -m 755 $(TARGET_LIB) $(PREFIX)/lib

jwtest: $(LYOBJS) $(OBJS) jwtest.o
	$(CXX) $(CFLAGS) -o $@ $^ 

threadhammer: $(LYOBJS) $(OBJS) $(TEST_OBJS)
	$(CXX) $(CFLAGS) -o $@ $^ -lpthread

libmicrojson.so: $(LYOBJS) $(OBJS)
	$(CXX) $(CFLAGS) -shared -o $@ $^

libmicrojson.a: $(LYOBJS) $(OBJS)
	$(AR) rcs $@ $^

JsonParser.cpp: json.yy
	bison  -o$@ $^

JsonScanner.cpp: json.l
	flex --outfile=$@ $^

JsonScanner.o: JsonScanner.cpp
	$(CXX) $(CFLAGS)  -c $^

JsonParser.o: JsonParser.cpp
	$(CXX) $(CFLAGS)  -c $^

%.o: %.cpp
	$(CXX) $(CFLAGS) -Werror -c $^


clean:
	rm -f *.o
	rm -f JsonParser.cpp
	rm -f JsonScanner.cpp
	rm -f *.hh
	rm -f *.hpp
	rm -f *.output
	rm -f threadhammer
	rm -f *.so
	rm -f *.a


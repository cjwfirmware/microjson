#ifndef JSON_BOOL_H
#define JSON_BOOL_H

#include "BaseJsonNode.h"

class JsonBool: public BaseJsonNode{
 public:
  JsonBool(bool pValue);
  JsonNodeType getType() const;
  std::string serialize();
  void setValue(bool pValue);
  bool value() const;
  BaseJsonNode *copyInternal() const;
 private:
  bool nodeEqual(BaseJsonNode *pNode);
  bool mValue;
};
  

#endif

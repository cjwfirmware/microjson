#ifndef JSON_OBJ_ENTRY_H
#define JSON_OBJ_ENTRY_H

#include "BaseJsonNode.h"
#include "JsonString.h"

class JsonObjEntry{
 public:
  JsonObjEntry(JsonString *pKey, BaseJsonNode *pValue);
  ~JsonObjEntry();
  JsonString *getKey();
  BaseJsonNode *getValue();
  std::string serialize();
  JsonObjEntry *copy(BaseJsonNode *pParent) const;
 private:
  JsonString *mKey;
  BaseJsonNode *mValue;
};

#endif

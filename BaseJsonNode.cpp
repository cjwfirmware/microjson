#include "BaseJsonNode.h"

bool BaseJsonNode::equals(BaseJsonNode *pNode){
  if(getType() != pNode->getType()){
    return false;
  }
  return nodeEqual(pNode);
}


BaseJsonNode *BaseJsonNode::copy(BaseJsonNode *pParent) const{
  switch(getType()){
  case JSON_STRING:
  case JSON_NUMBER:
  case JSON_BOOL:
  case JSON_NULL:
  case JSON_ROOT:
    return copyInternal();
  case JSON_OBJECT:
  case JSON_ARRAY:
    return copyInternal(pParent);
  }
  return NULL;
}

BaseJsonNode *BaseJsonNode::copyInternal() const{
  return NULL;
}

BaseJsonNode *BaseJsonNode::copyInternal(BaseJsonNode *pParent) const{
  return NULL;
}

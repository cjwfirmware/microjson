#include "uJson.h"


uJson::uJson(JsonRoot *pRoot){
  if(pRoot != NULL){
    mNode = pRoot->getNode();
  }else{
    mNode = NULL;
  }
}

uJson::uJson(){
  mNode = NULL;
}

int uJson::count(){
  if(mNode == NULL){ throw JsonNoObject; } 
  if(mNode->getType() != JSON_OBJECT && mNode->getType() != JSON_ARRAY){
    throw JsonTypeError;
    return 0;
  }
  return ((BaseJsonList*)mNode)->count();
}

JsonNodeType uJson::getType(){
  if(mNode == NULL){ throw JsonNoObject; } 
  return mNode->getType();
}

uJson uJson::operator[] (int pIndex){
  uJson jw;
  if(mNode == NULL){ throw JsonNoObject; } 
  if(mNode->getType() == JSON_OBJECT){
    jw.mNode = ((JsonObject*)mNode)->getIndex(pIndex);
    if(jw.mNode == NULL){
      throw JsonNoObject;
    }
  }else if(mNode->getType() == JSON_ARRAY){
    if(pIndex < 0 || pIndex >= (int)((JsonArray*)mNode)->count()){
      throw JsonOutOfBounds;
    }
    jw.mNode = ((JsonArray*)mNode)->get(pIndex);
  }else{
    throw JsonTypeError;
  }
  return jw;
}

uJson uJson::operator[] (std::string pName){
  uJson jw;
  if(mNode == NULL){ throw JsonNoObject; } 
  if(mNode->getType() == JSON_OBJECT){
    jw.mNode = ((JsonObject*)mNode)->get(pName);
  }else{
    throw JsonTypeError;
  }
  return jw;
}

uJson uJson::operator[] (const char *pName){
  uJson jw;
  if(mNode == NULL){ throw JsonNoObject; } 
  if(mNode->getType() == JSON_OBJECT){
    if(((JsonObject*)mNode)->contains(pName) == false){
      throw JsonNoObject;
    }
    jw.mNode = ((JsonObject*)mNode)->get(pName);
  }else{
    throw JsonTypeError;
  }
  return jw;
}

uJson::operator int() const{
  JsonNumber *jn;
  if(mNode == NULL){ throw JsonNoObject; } 
  if(mNode->getType() != JSON_NUMBER){
    throw JsonTypeError;
  }
  jn = (JsonNumber*)mNode;
  if(jn->isInt() == false){
    throw JsonTypeError;
  }
  return jn->ivalue();
}

uJson::operator double() const{
  JsonNumber *jn;
  if(mNode == NULL){ throw JsonNoObject; } 
  if(mNode->getType() != JSON_NUMBER){
    throw JsonTypeError;
  }
  jn = (JsonNumber*)mNode;
  if(jn->isInt() == true){
    return (double)jn->ivalue();
  }
  return jn->value();
}

uJson::operator std::string() const{
  if(mNode == NULL){ throw JsonNoObject; } 
  if(mNode->getType() != JSON_STRING){
    throw JsonTypeError;
 
  }
  return ((JsonString*)mNode)->value();
}

uJson::operator bool() const{
  if(mNode == NULL){ throw JsonNoObject; } 
  if(mNode->getType() != JSON_BOOL){
    throw JsonTypeError;
  }
  return ((JsonBool*)mNode)->value();
}

BaseJsonNode * uJson::getNode(){
  return mNode;
}

void uJson::cleanup(){
  BaseJsonList *bjl;
  bjl = dynamic_cast<BaseJsonList*>(mNode);
  if(bjl == NULL){
    return;
  }
  while(bjl->getParent()->getType() != JSON_ROOT){
    bjl = dynamic_cast<BaseJsonList*>(bjl->getParent());
    if(bjl == NULL){
      return;
    }
  }
  delete bjl->getParent();
  mNode = NULL;
}

int uJson::operator=(int pValue){
  if(mNode == NULL){ throw JsonNoObject; } 
  if(mNode->getType() != JSON_NUMBER){
    throw JsonTypeError;
  }
  ((JsonNumber*)mNode)->setValue(pValue);
  return pValue;
}
double uJson::operator=(double pValue){
  if(mNode == NULL){ throw JsonNoObject; } 
  if(mNode->getType() != JSON_NUMBER){
    throw JsonTypeError;
  }
  ((JsonNumber*)mNode)->setValue(pValue);
  return pValue;
}

bool uJson::operator=(bool pValue){
  if(mNode == NULL){ throw JsonNoObject; } 
  if(mNode->getType() != JSON_BOOL){
    throw JsonTypeError;
  }
  ((JsonBool*)mNode)->setValue(pValue);
  return pValue;  
}

std::string & uJson::operator=(std::string pValue){
  if(mNode == NULL){ throw JsonNoObject; } 
  if(mNode->getType() != JSON_STRING){
    throw JsonTypeError;
  }
  return ((JsonString*)mNode)->setValue(pValue);
}

std::string & uJson::operator=(const char *pValue){
  if(mNode == NULL){ throw JsonNoObject; } 
  if(mNode->getType() != JSON_STRING){
    throw JsonTypeError;
  }
  return ((JsonString*)mNode)->setValue(pValue);
}

std::string uJson::serialize(){
  if(mNode == NULL){ throw JsonNoObject; } 
  return mNode->serialize();
}

bool uJson::contains(std::string pKey){
  if(mNode == NULL){ throw JsonNoObject; } 
  if(mNode->getType() != JSON_OBJECT){
    throw JsonTypeError;
  }
  return ((JsonObject*)mNode)->contains(pKey);
}

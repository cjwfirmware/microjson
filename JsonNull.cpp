#include "JsonNull.h"

JsonNull::JsonNull(){}

JsonNodeType JsonNull::getType()  const{
  return JSON_NULL;
}

std::string JsonNull::serialize(){
  return std::string("null");
}

bool JsonNull::nodeEqual(BaseJsonNode *pNode){
  return true;
}

BaseJsonNode *JsonNull::copyInternal() const{
  return new JsonNull();
}

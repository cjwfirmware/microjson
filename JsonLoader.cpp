#include "JsonLoader.h"
#include "JsonDriver.h"

JsonRoot *loadJson(std::string &pInput){
  JSON::JsonDriver jd;
  return jd.parse(pInput);
}
JsonRoot *loadJson(const char *pInput){
  JSON::JsonDriver jd;
  return jd.parse(pInput);
}
JsonRoot *loadJsonFile(const char *pFilename){
  JSON::JsonDriver jd;
  return jd.parseFile(pFilename);
}

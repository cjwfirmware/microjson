#ifndef JSON_STRING_H
#define JSON_STRING_H

#include <string>
#include "BaseJsonNode.h"

class JsonString: public BaseJsonNode{
 public:
  JsonString(std::string &pValue);
  JsonString(const char *pValue);
  JsonNodeType getType() const ;
  std::string serialize();
  std::string & setValue(std::string pValue);
  std::string & setValue(const char *pValue);
  std::string value() const;
  std::string valueEscaped() const;
 private:
  BaseJsonNode *copyInternal() const;
  bool nodeEqual(BaseJsonNode *pNode);
  std::string mValue;
};


#endif

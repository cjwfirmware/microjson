#ifndef JSON_LOADER_H
#define JSON_LOADER_H

#include "JsonRoot.h"

JsonRoot * loadJson(std::string &pInput);
JsonRoot * loadJson(const char *pInput);
JsonRoot * loadJsonFile(const char *pFilename);

#endif

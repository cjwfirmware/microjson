#include "JsonObjEntry.h"

JsonObjEntry::JsonObjEntry(JsonString *pKey, BaseJsonNode *pValue){
  mKey = pKey;
  mValue = pValue;
}

JsonObjEntry::~JsonObjEntry(){
  delete mKey;
  delete mValue;
}

JsonString * JsonObjEntry::getKey(){
  return mKey;
}

BaseJsonNode * JsonObjEntry::getValue(){
  return mValue;
}

std::string JsonObjEntry::serialize(){
  std::string ret;
  ret = mKey->serialize() + ":" + mValue->serialize();
  return ret;
}


JsonObjEntry *JsonObjEntry::copy(BaseJsonNode *pParent) const{
  JsonObjEntry *je;
  je = new JsonObjEntry((JsonString*)mKey->copy(pParent), mValue->copy(pParent));
  return je;
}

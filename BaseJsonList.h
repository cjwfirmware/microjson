#ifndef BASE_JSON_LIST_H
#define BASE_JSON_LIST_H

#include <vector>
#include "BaseJsonNode.h"

class BaseJsonList: public BaseJsonNode{
 public:
  BaseJsonList(BaseJsonNode *pParent);
  BaseJsonNode *getParent();
  virtual unsigned int count() const = 0;
 private:  
  BaseJsonNode *mParent;
};
#endif

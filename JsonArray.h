#ifndef JSON_ARRAY_H
#define JSON_ARRAY_H



#include <vector>
#include "BaseJsonList.h"

class JsonArray: public BaseJsonList{
 public:
  JsonArray(BaseJsonNode *pParent);
  ~JsonArray();
  JsonNodeType getType() const;
  std::string serialize();
  unsigned int count() const;
  BaseJsonNode *get(unsigned int pIndex);
  void addEntry(BaseJsonNode *pNode);
 private:
  BaseJsonNode *copyInternal(BaseJsonNode *pParent) const;
  bool nodeEqual(BaseJsonNode *pNode);
  std::vector<BaseJsonNode*> mList;
};

#endif

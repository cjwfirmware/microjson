#ifndef JSON_NUMBER_H
#define JSON_NUMBER_H

#include <stdio.h>
#include "BaseJsonNode.h"

class JsonNumber: public BaseJsonNode{
 public:
  JsonNumber(double pValue);
  JsonNumber(int pValue);
  JsonNodeType getType() const;
  std::string serialize();
  void setValue(int pValue);
  void setValue(double pValue);
  double value() const;
  int ivalue() const;
  bool isInt() const;
 private:
  BaseJsonNode *copyInternal() const;
  bool nodeEqual(BaseJsonNode *pNode);
  double mValue;
  int mIValue;
  bool mInt;
};

#endif

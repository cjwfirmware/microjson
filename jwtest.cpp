#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "uJson.h"


void singleTest(){
  //const char *buf = "[ \"a1\", \"a2\", [\"b1\", 10, 10.5], \"a3\"]";
  const char *buf = "[ \"a1\", \"a2\", {\"foo\": \"77\"}, \"a3\"]";
  std::string inp;
  JsonRoot *jr;
  int s;
  std::string tmp;
  inp = std::string(buf);
  jr = loadJson(inp);
  inp = jr->serialize();
  printf("Result: %s\n", inp.c_str());
  uJson jw(jr);
  try{
    printf("%d\n", jw.count());
    printf("%d\n", jw[2]["foo"].getType());
    tmp = std::string(jw[2]["foo"]);
    printf("%d\n", s);
    printf("%s\n", tmp.c_str());
  }
  catch(BaseJsonException& bje){
    printf("Error: %s\n", bje.errstr());
  }
}



int main(int argc, char **argv){
  singleTest();
}

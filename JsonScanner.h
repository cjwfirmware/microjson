#ifndef JSON_SCANNER_H
#define JSON_SCANNER_H

#if ! defined(yyFlexLexerOnce)
#define yyFlexLexer JsonFlexLexer
#include <FlexLexer.h>
#undef yyFlexLexer
#endif

#include "JsonParser.hpp"
#include "location.hh"

namespace JSON{
  class JsonScanner: public JsonFlexLexer{
  public:
    JsonScanner(std::istream *in): JsonFlexLexer(in){}
    virtual ~JsonScanner(){}
    using FlexLexer::yylex;
    int yylex(JSON::JsonParser::semantic_type *const lval, JSON::JsonParser::location_type *location);
  private:
    JSON::JsonParser::semantic_type *yylval = NULL;
  };
}

#endif

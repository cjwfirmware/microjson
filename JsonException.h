#ifndef JSON_EXCEPTION_H
#define JSON_EXCEPTION_H

#include <exception>


class BaseJsonException : public std::exception{
 public:
  virtual const char *errstr() const throw() = 0;
};

class _JsonOutOfBounds: public BaseJsonException{
 public:
  virtual const char *errstr() const throw(){
    return "Out of bounds exception";
  }
};

class _JsonTypeError: public BaseJsonException{
 public:
  virtual const char *errstr() const throw(){
    return "Json Type Error";
  }
};

class _JsonNoObject: public BaseJsonException{
  public:
  virtual const char *errstr() const throw(){
    return "Json object does not contain entry";
  }
};

extern _JsonOutOfBounds JsonOutOfBounds;
extern _JsonTypeError JsonTypeError;
extern _JsonNoObject JsonNoObject;

#endif
